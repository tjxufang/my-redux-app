import SongList from "./components/songList";
import SongDetail from "./components/songDetail";

function App() {
    return (
        <div>
            <SongDetail/>
            <br/>
            <SongList/>
        </div>
    )
}

export default App
