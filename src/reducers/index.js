import {combineReducers} from "redux"
import {SONG_FETCHED, SONG_SELECTED} from "../helper";
import userReducer from "./userReducer";

// const songsReducer = () => {
//     return [
//         {title: "Song 1", duration: "2.37"},
//         {title: "Song 2", duration: "6.88"},
//         {title: "Song 3", duration: "2.27"},
//         {title: "Song 4", duration: "3.15"},
//     ]
// }

const selectedSongReducer = (state = null, action) => {
    if (action.type === SONG_SELECTED){
        return action.payload
    }
    return state
}

const fetchSongReducer = (state = [], action) => {
    if (action.type === SONG_FETCHED) {
        return action.payload.data
    }
    return state
}

export default combineReducers({
    selectedSongReducer,
    fetchSongReducer,
    userReducer,
})