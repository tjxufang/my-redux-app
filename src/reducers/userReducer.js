import {USER_FETCHED} from "../helper";

const initState = {
    allUsers: [],
}

export default (state = initState, action) => {
    switch (action.type) {
        case USER_FETCHED:
            return {...state, allUsers: action.payload.data}
        default:
            return state
    }
}