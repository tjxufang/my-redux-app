import {SONG_FETCHED, SONG_SELECTED, USER_FETCHED} from "../helper";
import jsonPH from "../api/jsonPH";


const actSelectSong = (song) => {
    return {
        type: SONG_SELECTED,
        payload: song,
    }
}

// export const actFetchSong = async () => {
//     let songs = await axios.get(songURL)
//     return {
//         type: 'SONG_FETCHED',
//         payload: songs
//     }
// }
export const actFetchSong = () => (
    async dispatch => {
        let songs = await jsonPH.get('albums')
        dispatch ({
            type: SONG_FETCHED,
            payload: songs
        })
    }
)

export const actFetchUser = () => (
    async dispatch => {
        let users = await jsonPH.get('users')
        dispatch ({
            type: USER_FETCHED,
            payload: users
        })
    }
)

export default actSelectSong