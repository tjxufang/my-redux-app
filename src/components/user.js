import React from 'react';
import {connect} from 'react-redux'

class User extends React.Component {
    render() {
        return (
            <div>
                <h3>Artist: {this.props.user?.name}</h3>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        user: state.userReducer.allUsers.find(user => user.id === ownProps.userId)
    }
}


export default connect(mapStateToProps, {})(User)