import React, {Component} from 'react'
import {connect} from 'react-redux'
import actSelectSong, {actFetchSong, actFetchUser} from "../actions";
import User from "./user";

class SongList extends Component {
    componentDidMount(){
        this.props.actFetchSong()
        this.props.actFetchUser()
    }

    renderSongList() {
        return this.props.songs.map((song, i) => {
            return (
                <div key={i}>
                    <User userId={song.userId}/>
                    <button onClick={() => this.props.actSelectSong(song)}>{song.title}</button>
                </div>
            )
        })
    }

    render() {
        return (
            <div>
                {this.renderSongList()}
            </div>
        )
    }
}

const mapStateToProps = state => {
    console.log('state', state)
    return {
        songs: state.fetchSongReducer,
    }
}

export default connect(mapStateToProps, {actSelectSong, actFetchSong, actFetchUser})(SongList)